# KekDB
An alternative

<br>

## Need for speed?
Then consider KekDB. KekDB is an database founded by [KekOnTheWorld](https://gitlab.com/KekOnTheWorld) and [CraftingDragon007](https://gitlab.com/CraftingDragon007).

<br>

## How does it work?
It is hashing the database objects so you can fetch updates only when you need to!

<br>

## Example
Heres one in java:
```
class User {
    public UUID uuid;
    public String name;
    public int age;

    public User(String name, int age) {
        this.name = name;
        this.age = age;
    }
}

ObjectManager<UUID, User> users = ObjectManager.asign<UUID, User>(row -> {
    return new User(row.getIdentifier(), row.getString("name"), row.getInt("age"));
}, STRING, "name", VARINT, "age");

// Using cache
User user1 = users.getCached(UUID.randomUUID());
User user2 = users.getCachedOrCreate(UUID.randomUUID(), "Peter", 10);
User user3 = users.filterCached(u->u.age<10).findFirst();
User user4 = users.create(UUID.randomUUID(), "Kekw", 29);

// Using noncache
User user1 = users.get(UUID.randomUUID());
User user2 = users.getOrCreate(UUID.randomUUID(), "Peter", 10);
User user3 = users.filter(u->u.age<10).findFirst();
User user4 = users.create(UUID.randomUUID(), "Kekw", 29);

// Lets say we want to update cached values (It already updates every x minutes)
users.update();

// Or just one by uuid
users.update(UUID.randomUUID());
```

<br>

## Building using CMake
```
mkdir build
cd build
cmake ..
cmake --build .
```